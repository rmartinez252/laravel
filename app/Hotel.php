<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    //
    public function rooms(){
        return $this->hasMany('App\Room')->where('status_id','<>',3)->orderBy('price','ASC');
    }
    public function fullRooms(){
        return $this->hasMany('App\Room')->orderBy('status_id','ASC')->orderBy('price','ASC');
    }
}
