<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Hotel;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class BookingController extends Controller
{
    /**
     * BookingController constructor.
     */
    public function __construct()
    {

        $this->middleware('auth');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm($id_room)
    {
        $room = Room::find($id_room);
        return view('booking')->with(array(
            'room'=>$room
        ));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @param  Request $request
     * @param  integer  $id_room
     */
    public function createSave(Request $request ,$id_room)
    {
        $user = Auth::user();
        $room = Room::find($id_room);
        $booking = new Booking();
        $booking->user_id = $user->id;
        $booking->room_id = $room->id;
        $booking->date_start = $request->input('date_start');
        $booking->date_end = $request->input('date_end');
        $booking->save();
        $room->status_id = 2;
        $room->save();

        return Redirect::route('hotels.index');
        //
    }

}
