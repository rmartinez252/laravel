<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //
    public function hotel(){
        return $this->belongsTo('App\Hotel');
    }
    public function status(){
        return $this->belongsTo('App\RoomStatus');
    }
    public function bokings(){
        return $this->hasMany('App\Booking');
    }
}
