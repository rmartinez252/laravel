@extends('layouts.base')

@section('body')

    <!-- end row -->
    Room List

    <div class="row">
        <div class="col-md-12">
            {{$hotel->name}}
        </div>
    </div>
    <div class="row">

        <div class="col-sm-12">

            <div class="well padding-10">
                @foreach($rooms as $room)
                    <div class="row">
                        <div class="col-md-3">
                            <img src="{{asset('img/room/'.$room->id.'.jpg')}}" class="img-responsive" alt="img">
                            <ul class="list-inline padding-10">

                            </ul>
                        </div>
                        <div class="col-md-6 padding-left-0">
                            <h3 class="margin-top-0"><a>{{$room->name}}</a> <br><small class="font-xs">{{$room->status->name}}</small></h3>
                            @if($room->status->name != 'SOLD OUT' )
                                <a class="btn btn-warning" href="/book/new/{{$room->id}}">
                                    <i class="fa fa-arrow-down"></i>
                                    book room
                                </a>
                            @endif
                        </div>
                        <div class="col-md-3">
                            Price: {{$room->price}}

                        </div>
                    </div>
                    <hr>
                @endforeach


            </div>

        </div>



    </div>

@endsection