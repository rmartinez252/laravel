@extends('layouts.base')

@section('body')

    <!-- end row -->
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">{{ __('Booking Room') }} {{$room->name}}</div>

                <div class="card-body">
                    <form class="smart-form" action="/book/new/{{$room->id}}" method="POST">

                        @csrf
                        <div class="form-group row">
                            <label for="date_start" class="col-md-4 col-form-label text-md-right">{{ __('Date Start') }}</label>

                            <div class="col-md-6">
                                <input id="date_start" type="date" class="form-control{{ $errors->has('date_start') ? ' is-invalid' : '' }}" name="date_start" value="{{ old('date_start') }}" required autofocus>

                                @if ($errors->has('date_start'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('date_start') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date_end" class="col-md-4 col-form-label text-md-right">{{ __('Date Start') }}</label>

                            <div class="col-md-6">
                                <input id="date_end" type="date" class="form-control{{ $errors->has('date_end') ? ' is-invalid' : '' }}" name="date_end" value="{{ old('date_end') }}" required autofocus>

                                @if ($errors->has('date_end'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('date_end') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Book') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div>



    </div>

@endsection