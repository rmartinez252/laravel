@extends('layouts.base')

@section('body')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Lastname') }}</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autofocus>

                                @if ($errors->has('lastname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="cc_number" class="col-md-4 col-form-label text-md-right">{{ __('Credit card number') }}</label>

                            <div class="col-md-6">
                                <input id="cc_number" type="text" class="form-control{{ $errors->has('cc_number') ? ' is-invalid' : '' }}" name="cc_number" value="{{ old('cc_number') }}" required>

                                @if ($errors->has('cc_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cc_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cc_expiration_date" class="col-md-4 col-form-label text-md-right">{{ __('Credit Card Expiration date') }}</label>

                            <div class="col-md-6">
                                <input id="cc_expiration_date" type="text" class="form-control{{ $errors->has('cc_expiration_date') ? ' is-invalid' : '' }}" name="cc_expiration_date" value="{{ old('cc_expiration_date') }}" required>

                                @if ($errors->has('cc_expiration_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cc_expiration_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cc_security_code" class="col-md-4 col-form-label text-md-right">{{ __('Credit security code') }}</label>

                            <div class="col-md-6">
                                <input id="cc_security_code" type="text" class="form-control{{ $errors->has('cc_security_code') ? ' is-invalid' : '' }}" name="cc_security_code" value="{{ old('cc_security_code') }}" required>

                                @if ($errors->has('cc_security_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cc_security_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
