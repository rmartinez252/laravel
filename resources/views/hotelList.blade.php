
@extends('layouts.base')

@section('body')

    <!-- end row -->
    @if(count($hotels))

        <div class="row">

            <div class="col-sm-12">

                <div class="well padding-10">
                    @foreach($hotels as $hotel)
                        <div class="row">
                            <div class="col-md-3">
                                <img src="img/hotel/{{$hotel->id}}.jpg" class="img-responsive" alt="img">
                                <ul class="list-inline padding-10">
                                    {{--<li>--}}
                                    {{--<i class="fa fa-calendar"></i>--}}
                                    {{--<a href="javascript:void(0);"> March 12, 2015 </a>--}}
                                    {{--</li>--}}
                                    <li>
                                        @for($i=1;$i<=5;$i++)
                                            @if($i<= $hotel->stars)
                                                <i class="fa fa-star"></i>
                                            @else
                                                <i class="fa fa-star-o"></i>
                                            @endif
                                        @endfor
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6 padding-left-0">
                                <h3 class="margin-top-0"><a>{{$hotel->name}}</a> <br><small class="font-xs">{{$hotel->address }}</small></h3>

                                @if($hotel->rooms->first())
                                    <a class="btn btn-warning" href="/hotels/{{$hotel->id}}">
                                        <i class="fa fa-arrow-down"></i>
                                        Check Available Rooms
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-3">
                                @if($hotel->rooms->first())
                                    Price : {{$hotel->rooms->first()->price}}
                                @else
                                    No Room Available
                                @endif
                            </div>
                        </div>
                        <hr>
                    @endforeach
                    {{$hotels->links()}}


                </div>

            </div>



        </div>
    @endif

@endsection