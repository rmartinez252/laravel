<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HotelsController@index');
Route::get('/room', function () {
    return view('room');
});

Route::resource('hotels','HotelsController');

Auth::routes();
Route::get('/book/new/{room_id}', 'BookingController@createForm');
Route::post('/book/new/{room_id}', 'BookingController@createSave');

