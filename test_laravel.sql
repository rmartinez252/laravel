/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : test_laravel

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-08-04 16:34:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `bookings`
-- ----------------------------
DROP TABLE IF EXISTS `bookings`;
CREATE TABLE `bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of bookings
-- ----------------------------
INSERT INTO `bookings` VALUES ('1', '2018-08-04 19:33:51', '2018-08-04 19:33:51', '5', '1', '2018-12-31', '2018-12-31');
INSERT INTO `bookings` VALUES ('2', '2018-08-04 19:36:28', '2018-08-04 19:36:28', '5', '1', '2018-12-31', '2018-12-31');
INSERT INTO `bookings` VALUES ('3', '2018-08-04 19:36:37', '2018-08-04 19:36:37', '5', '1', '2018-12-31', '2018-12-31');
INSERT INTO `bookings` VALUES ('4', '2018-08-04 19:37:16', '2018-08-04 19:37:16', '1', '1', '2018-12-31', '2018-12-31');

-- ----------------------------
-- Table structure for `hotels`
-- ----------------------------
DROP TABLE IF EXISTS `hotels`;
CREATE TABLE `hotels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stars` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of hotels
-- ----------------------------
INSERT INTO `hotels` VALUES ('1', 'Hotel name 1', 'address 02', '4', null, null);
INSERT INTO `hotels` VALUES ('2', 'Holte name -2', 'far far away', '5', null, null);
INSERT INTO `hotels` VALUES ('3', 'hotel name 03', 'not so far', '3', null, null);

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2018_07_28_141232_create_hotels_table', '1');
INSERT INTO `migrations` VALUES ('4', '2018_07_28_153613_create_rooms_table', '1');
INSERT INTO `migrations` VALUES ('5', '2018_07_28_153639_create_room_statuses_table', '1');
INSERT INTO `migrations` VALUES ('6', '2018_07_28_153702_create_search_logs_table', '1');
INSERT INTO `migrations` VALUES ('7', '2018_07_28_154635_create_bookings_table', '1');
INSERT INTO `migrations` VALUES ('8', '2018_07_28_161831_add_hotel_id_to_room', '1');
INSERT INTO `migrations` VALUES ('9', '2018_07_28_164629_add_status_id_to_rooms', '1');
INSERT INTO `migrations` VALUES ('10', '2018_07_28_164651_add_room_id_to_booking', '1');
INSERT INTO `migrations` VALUES ('11', '2018_07_28_164704_add_user_id_to_booking', '1');
INSERT INTO `migrations` VALUES ('12', '2018_07_28_164726_add_user_id_to_search_log', '1');

-- ----------------------------
-- Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for `rooms`
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hotel_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of rooms
-- ----------------------------
INSERT INTO `rooms` VALUES ('1', 'room 01', '150.00', null, '2018-08-04 19:37:16', '1', '2');
INSERT INTO `rooms` VALUES ('2', 'room 02', '50.00', null, null, '1', '3');
INSERT INTO `rooms` VALUES ('3', 'room', '200.00', null, null, '1', '2');
INSERT INTO `rooms` VALUES ('4', 'room', '500.00', null, null, '2', '2');
INSERT INTO `rooms` VALUES ('5', 'roomname', '600.00', null, '2018-08-04 19:33:51', '2', '2');
INSERT INTO `rooms` VALUES ('6', 'roomn ', '50.00', null, null, '3', '1');
INSERT INTO `rooms` VALUES ('7', 'room3', '70.00', null, null, '3', '2');

-- ----------------------------
-- Table structure for `room_statuses`
-- ----------------------------
DROP TABLE IF EXISTS `room_statuses`;
CREATE TABLE `room_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of room_statuses
-- ----------------------------
INSERT INTO `room_statuses` VALUES ('1', 'AVAILABLE', null, null);
INSERT INTO `room_statuses` VALUES ('2', 'ON REQUEST', null, null);
INSERT INTO `room_statuses` VALUES ('3', 'SOLD OUT', null, null);

-- ----------------------------
-- Table structure for `search_logs`
-- ----------------------------
DROP TABLE IF EXISTS `search_logs`;
CREATE TABLE `search_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `search` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of search_logs
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cc_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cc_expiration_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cc_security_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'ronald', 'asd', 'asd22@asd.c', '$2y$10$84c/pUGUC4hO1LLiF5vfxeALM8LOQ5JkGulS5AGB18RDAxgCEecue', 'asd', 'asd', 'ads', 'ZQNgusDzy0h8eYJaOI2LcYgoCfHN1eVwbPqQg5viyRqsTiVKnKIJgTRnQvTe', '2018-08-04 18:59:28', '2018-08-04 18:59:28');
